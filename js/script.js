window.onload = function () {
document.querySelector('.tabs').addEventListener('click', fTabs);
function fTabs(event) {
    if (event.target.className === 'tabs-title'){
        let dataTab = event.target.getAttribute('data-tab');
        let tabH = document.getElementsByClassName('tabs-title');
        for (let i=0; i < tabH.length; i++){
            tabH[i].classList.remove('active');
        }
        event.target.classList.add('active');
        let tabBody = document.getElementsByClassName('tabs-cont');
        for (let i =0; i < tabBody.length; i++){
            if (dataTab == i){
                tabBody[i].style.display = 'block'
            }
            else {
                tabBody[i].style.display = 'none'
            }
        }
    }
}
};